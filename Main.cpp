//Playing Cards
//Jason Le

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

struct Card
{
	Rank rank;
	Suit suit;
};

enum Rank
{
	two = 2, three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace
};

enum Suit
{
	heart, diamond, club, spade
};

int main()
{

	_getch();
	return 0;
}